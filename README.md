# Dependency:
xsettingsd

## Optional:
`darkreader` plugin for Firefox. Websites can follow the systems dark/light theme directly.

# Further reading
[ArchWiki about uniform styling of GTK+QT](https://wiki.archlinux.org/title/uniform_look_for_Qt_and_GTK_applications)